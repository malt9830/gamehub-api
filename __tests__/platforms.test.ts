import { AxiosResponse } from 'axios'
import { Platform, IPlatform } from '../models'
import { platforms } from '../data'
import apiClient from './apiClient'

interface PlatformsResponse extends AxiosResponse {
  data: {
    count: number
    results: IPlatform[]
  }
}

interface PlatformByIdResponse extends AxiosResponse {
  data: IPlatform
}

describe('/platforms', () => {
  let res: PlatformsResponse

  beforeAll(async () => {
    res = await apiClient.get('/platforms')
  })

  it('should return the platforms from the seeding', async () => {
    expect(res.status).toBe(200)
    expect(res.data.count).toBe(platforms.length)
  })

  it('should return an array', async () => {
    expect(res.status).toBe(200)
    expect(Array.isArray(res.data.results)).toBe(true)
  })
})

describe('/platforms when error', () => {
  it('should return status code 500 due to database error', async () => {
    jest.spyOn(Platform, 'find').mockRejectedValue(new Error('Database error'))

    try {
      await apiClient.get('/platforms')
    } catch (err: any) {
      expect(err.response.status).toBe(500)
    }
  })
})

describe('/platforms/:id when a platform is found', () => {
  let res: PlatformByIdResponse

  beforeAll(async () => {
    res = await apiClient.get(`/platforms/${platforms[0]._id}`)
  })

  it('should return the requested platform', async () => {
    expect(res.status).toBe(200)
    expect(res.data._id).toBe(platforms[0]._id)
  })
})

describe('/platforms/:id when no platform is found', () => {
  it('should return status code 404', async () => {
    try {
      await apiClient.get('/platforms/65ea65ea65ea65ea65ea65ea')
    } catch (err: any) {
      expect(err.response.status).toBe(404)
    }
  })
})

describe('/platforms/:id when error', () => {
  it('should return status code 500', async () => {
    try {
      await apiClient.get('/platforms/0')
    } catch (err: any) {
      expect(err.response.status).toBe(500)
    }
  })
})
