import axios from 'axios'
import 'dotenv/config'

const apiClient = axios.create({
  baseURL: process.env.TESTING_URI,
})

export default apiClient
