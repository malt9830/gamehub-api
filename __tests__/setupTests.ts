import mongoose from 'mongoose'
import 'dotenv/config'
import { Game, Genre, Platform } from '../models'
import { games, genres, platforms } from '../data'

// Define global setup
beforeAll(async () => {
  await mongoose.connect(process.env.TESTING_DB_URI as string)
  await Game.insertMany(games)
  await Genre.insertMany(genres)
  await Platform.insertMany(platforms)
})

// Define global teardown
afterAll(async () => {
  await mongoose.connection.db.dropDatabase()
  await mongoose.connection.close()
})
