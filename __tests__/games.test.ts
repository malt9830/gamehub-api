import { AxiosResponse } from 'axios'
import { Game, IGame } from '../models'
import { games, genres, platforms } from '../data'
import apiClient from './apiClient'

interface GamesResponse extends AxiosResponse {
  data: {
    count: number
    results: IGame[]
  }
}

interface GameByIdResponse extends AxiosResponse {
  data: IGame
}

describe('/games', () => {
  let res: GamesResponse

  beforeAll(async () => {
    res = await apiClient.get('/games')
  })

  it('should return list of games', async () => {
    expect(res.status).toBe(200)
    expect(res.data.count).toBe(games.length)
  })

  it('should return an array', async () => {
    expect(res.status).toBe(200)
    expect(Array.isArray(res.data.results)).toBe(true)
  })
})

describe('/games with query params', () => {
  it('should return games of parameterised genre', async () => {
    const genreID = genres[0]._id

    const res: GamesResponse = await apiClient.get('/games', {
      params: {
        genre: genreID,
      },
    })

    expect(res.status).toBe(200)
    expect(res.data.results.every((game) => game.genres?.some((genre) => genre._id === genreID))).toBe(true)
  })

  it('should return games of parameterised platform', async () => {
    const platformID = platforms[0]._id

    const res: GamesResponse = await apiClient.get('/games', {
      params: {
        platform: platformID,
      },
    })

    expect(res.status).toBe(200)
    expect(res.data.results.every((game) => game.platforms?.some((platform) => platform._id === platformID))).toBe(true)
  })
})

describe('/games when error', () => {
  it('should return status code 500 due to database error', async () => {
    jest.spyOn(Game, 'find').mockRejectedValue(new Error('Database error'))

    try {
      await apiClient.get('/games')
    } catch (err: any) {
      expect(err.response.status).toBe(500)
    }
  })
})

describe('/games/:id when a game is found', () => {
  let res: GameByIdResponse

  beforeAll(async () => {
    res = await apiClient.get(`/games/${games[0]._id}`)
  })

  it('should return the requested game', async () => {
    expect(res.status).toBe(200)
    expect(res.data._id).toBe(games[0]._id)
  })

  it('should have genres array', async () => {
    expect(res.status).toBe(200)
    expect(Array.isArray(res.data.genres)).toBe(true)
  })

  it('should have platforms array', async () => {
    expect(res.status).toBe(200)
    expect(Array.isArray(res.data.platforms)).toBe(true)
  })
})

describe('/games/:id when no game is found', () => {
  it('should return status code 404', async () => {
    try {
      await apiClient.get('/games/65ea65ea65ea65ea65ea65ea')
    } catch (err: any) {
      expect(err.response.status).toBe(404)
    }
  })
})

describe('/games/:id when error', () => {
  it('should return status code 500', async () => {
    try {
      await apiClient.get('/games/0')
    } catch (err: any) {
      expect(err.response.status).toBe(500)
    }
  })
})
