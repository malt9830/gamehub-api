import games from './games'
import genres from './genres'
import platforms from './platforms'

export { games, genres, platforms }
