#!/bin/sh

case "$RTE" in
    dev )
        echo "Running: development mode"
        npm run dev
        ;;
    test )
        echo "Running: test mode"
        npm audit || exit 1
        npm run test
        ;;
    prod )
        echo "Running: production mode"
        npm run start
        ;;
    * )
        echo "Unknown runtime environment"
        exit 1
        ;;
esac
