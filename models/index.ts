export { default as Game, IGame } from './game'
export { default as Genre, IGenre } from './genre'
export { default as Platform, IPlatform } from './platform'
