import setupRouters from './startup/setupRouters'
import express from 'express'
import cors from 'cors'

const app = express()

app.use(express.json())
app.use(cors())

setupRouters(app)

export default app
