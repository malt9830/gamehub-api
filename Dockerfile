FROM node:20-slim

WORKDIR /app/

RUN \
    --mount=type=bind,source=package.json,target=package.json \
    --mount=type=bind,source=package-lock.json,target=package-lock.json \
    --mount=type=cache,target=/root/.npm \
    npm ci


COPY . .

EXPOSE 3000

ENTRYPOINT ["sh", "entrypoint.sh"]
