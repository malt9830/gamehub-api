import app from './app'
import dbConnect from './startup/dbConnect'
import 'dotenv/config'

dbConnect()

app.listen(3000, () => {
  console.log('Server is running on port 3000')
})
